Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: Contributors to the OpenEXR Project.
License: BSD-3-clause

Files: ASWF/*
Copyright: no-info-found
License: BSD-3-clause

Files: IlmBase/Half/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/Iex/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/IexMath/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/IexTest/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/IlmThread/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/Imath/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/ImathTest/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: IlmBase/m4/ax_cxx_compile_stdcxx.m4
Copyright: 2016, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: OpenEXR/IlmImf/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImf/ImfCompositeDeepScanLine.cpp
 OpenEXR/IlmImf/ImfCompositeDeepScanLine.h
 OpenEXR/IlmImf/ImfDeepCompositing.cpp
 OpenEXR/IlmImf/ImfDeepCompositing.h
 OpenEXR/IlmImf/ImfFloatVectorAttribute.h
 OpenEXR/IlmImf/ImfMultiView.cpp
 OpenEXR/IlmImf/ImfMultiView.h
 OpenEXR/IlmImf/ImfPartHelper.h
 OpenEXR/IlmImf/ImfStringVectorAttribute.cpp
 OpenEXR/IlmImf/ImfStringVectorAttribute.h
Copyright: 2007, 2012, 2013, Weta Digital Ltd
License: BSD-3-clause

Files: OpenEXR/IlmImf/ImfDwaCompressor.cpp
 OpenEXR/IlmImf/ImfDwaCompressor.h
 OpenEXR/IlmImf/ImfDwaCompressorSimd.h
 OpenEXR/IlmImf/ImfFastHuf.cpp
 OpenEXR/IlmImf/ImfFastHuf.h
 OpenEXR/IlmImf/ImfSystemSpecific.cpp
 OpenEXR/IlmImf/dwaLookups.cpp
Copyright: 2009-2014, DreamWorks Animation LLC.
License: BSD-3-clause

Files: OpenEXR/IlmImf/ImfForward.h
 OpenEXR/IlmImf/ImfMultiPartOutputFile.h
Copyright: 2012, Weta Digital Ltd
 2011, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImf/ImfOptimizedPixelReading.h
 OpenEXR/IlmImf/ImfSimd.h
Copyright: 2012, Autodesk, Inc.
License: BSD-3-clause

Files: OpenEXR/IlmImf/ImfPxr24Compressor.cpp
 OpenEXR/IlmImf/ImfPxr24Compressor.h
Copyright: 2004, Pixar Animation Studios
License: BSD-3-clause

Files: OpenEXR/IlmImfExamples/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImfFuzzTest/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImfTest/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImfTest/compareDwa.cpp
 OpenEXR/IlmImfTest/compareDwa.h
 OpenEXR/IlmImfTest/testDwaCompressorSimd.cpp
 OpenEXR/IlmImfTest/testDwaCompressorSimd.h
 OpenEXR/IlmImfTest/testRle.cpp
 OpenEXR/IlmImfTest/testRle.h
Copyright: 2009-2014, DreamWorks Animation LLC.
License: BSD-3-clause

Files: OpenEXR/IlmImfTest/testBadTypeAttributes.cpp
 OpenEXR/IlmImfTest/testBadTypeAttributes.h
 OpenEXR/IlmImfTest/testCompositeDeepScanLine.cpp
 OpenEXR/IlmImfTest/testCompositeDeepScanLine.h
 OpenEXR/IlmImfTest/testDeepScanLineMultipleRead.cpp
 OpenEXR/IlmImfTest/testDeepScanLineMultipleRead.h
 OpenEXR/IlmImfTest/testFutureProofing.h
 OpenEXR/IlmImfTest/testLargeDataWindowOffsets.cpp
 OpenEXR/IlmImfTest/testLargeDataWindowOffsets.h
 OpenEXR/IlmImfTest/testOptimizedInterleavePatterns.cpp
 OpenEXR/IlmImfTest/testOptimizedInterleavePatterns.h
 OpenEXR/IlmImfTest/testPartHelper.cpp
 OpenEXR/IlmImfTest/testPartHelper.h
Copyright: 2007, 2012, 2013, Weta Digital Ltd
License: BSD-3-clause

Files: OpenEXR/IlmImfTest/testCopyDeepScanLine.cpp
 OpenEXR/IlmImfTest/testCopyDeepScanLine.h
 OpenEXR/IlmImfTest/testCopyMultiPartFile.h
Copyright: 2012, Weta Digital Ltd
 2011, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImfTest/testMultiView.cpp
 OpenEXR/IlmImfTest/testMultiView.h
Copyright: 2012, Industrial Light & Magic, a division of Lucasfilm
 2007, Weta Digital Ltd
License: BSD-3-clause

Files: OpenEXR/IlmImfUtil/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/IlmImfUtilTest/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/PATENTS
Copyright: able works distributedDreamWorks Animation as part of the OpenEXR
License: Apache-2.0

Files: OpenEXR/exr2aces/main.cpp
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrenvmap/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrheader/main.cpp
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrmakepreview/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrmaketiled/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrmultipart/exrmultipart.cpp
Copyright: heldothers as indicated. / 2012, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrmultiview/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/exrstdattr/main.cpp
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR/m4/ax_cxx_compile_stdcxx.m4
Copyright: 2016, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: OpenEXR_Viewers/exrdisplay/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: OpenEXR_Viewers/m4/ax_cxx_compile_stdcxx.m4
Copyright: 2016, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: OpenEXR_Viewers/playexr/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: PyIlmBase/PyIex/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: PyIlmBase/PyImath/*
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: PyIlmBase/PyImathNumpy/imathnumpymodule.cpp
Copyright: 1997-2018, Industrial Light & Magic, a division of Lucas
License: BSD-3-clause

Files: PyIlmBase/m4/ax_cxx_compile_stdcxx.m4
Copyright: 2016, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: README.md
Copyright: no-info-found
License: BSD-3-clause

Files: debian/*
Copyright: 2007-2008 Adeodato Simó <dato@debian.org>
           2009 Cyril Brulebois <kibi@debian.org>
           2014 Mathieu Malaterre <malat@debian.org>
           2014 Matteo F. Vescovi <mfv@debian.org>
License: ilmbase

Files: debian/rules
Copyright: 2009, Cyril Brulebois <kibi@debian.org>
License: ilmbase

Files: .github/* ASWF/tsc-meetings/* CMakeLists.txt CONTRIBUTING.md sonar-project.properties CMakeLists.txt CONTRIBUTING.md sonar-project.properties CMakeLists.txt CONTRIBUTING.md sonar-project.properties Contrib/* IlmBase/* IlmBase/Half/CMakeLists.txt IlmBase/Half/Makefile.am IlmBase/Half/CMakeLists.txt IlmBase/Half/Makefile.am IlmBase/Iex/CMakeLists.txt IlmBase/Iex/Makefile.am IlmBase/Iex/CMakeLists.txt IlmBase/Iex/Makefile.am IlmBase/IexMath/CMakeLists.txt IlmBase/IexMath/Makefile.am IlmBase/IexMath/CMakeLists.txt IlmBase/IexMath/Makefile.am IlmBase/IexTest/CMakeLists.txt IlmBase/IexTest/Makefile.am IlmBase/IexTest/CMakeLists.txt IlmBase/IexTest/Makefile.am IlmBase/IlmThread/CMakeLists.txt IlmBase/IlmThread/Makefile.am IlmBase/IlmThread/CMakeLists.txt IlmBase/IlmThread/Makefile.am IlmBase/Imath/CMakeLists.txt IlmBase/Imath/ImathExc.cpp IlmBase/Imath/Makefile.am IlmBase/Imath/CMakeLists.txt IlmBase/Imath/ImathExc.cpp IlmBase/Imath/Makefile.am IlmBase/Imath/CMakeLists.txt IlmBase/Imath/ImathExc.cpp IlmBase/Imath/Makefile.am IlmBase/ImathTest/CMakeLists.txt IlmBase/ImathTest/Makefile.am IlmBase/ImathTest/CMakeLists.txt IlmBase/ImathTest/Makefile.am OpenEXR/* OpenEXR/IlmImf/CMakeLists.txt OpenEXR/IlmImf/Makefile.am OpenEXR/IlmImf/CMakeLists.txt OpenEXR/IlmImf/Makefile.am OpenEXR/IlmImf/ImfRgbaYca.cpp OpenEXR/IlmImf/ImfRgbaYca.h OpenEXR/IlmImf/ImfRgbaYca.cpp OpenEXR/IlmImf/ImfRgbaYca.h OpenEXR/IlmImfExamples/CMakeLists.txt OpenEXR/IlmImfExamples/Makefile.am OpenEXR/IlmImfExamples/CMakeLists.txt OpenEXR/IlmImfExamples/Makefile.am OpenEXR/IlmImfFuzzTest/CMakeLists.txt OpenEXR/IlmImfFuzzTest/Makefile.am OpenEXR/IlmImfFuzzTest/CMakeLists.txt OpenEXR/IlmImfFuzzTest/Makefile.am OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/CMakeLists.txt OpenEXR/IlmImfTest/Makefile.am OpenEXR/IlmImfTest/TestUtilFStream.h OpenEXR/IlmImfTest/random.cpp OpenEXR/IlmImfTest/random.h OpenEXR/IlmImfTest/testB44ExpLogTable.cpp OpenEXR/IlmImfTest/testB44ExpLogTable.h OpenEXR/IlmImfTest/testDwaLookups.cpp OpenEXR/IlmImfTest/testDwaLookups.h OpenEXR/IlmImfTest/bswap_32.h OpenEXR/IlmImfTest/comp_b44.exr OpenEXR/IlmImfTest/comp_b44_piz.exr OpenEXR/IlmImfTest/comp_dwaa_piz.exr OpenEXR/IlmImfTest/comp_dwab_piz.exr OpenEXR/IlmImfTest/comp_dwaa_piz.exr OpenEXR/IlmImfTest/comp_dwab_piz.exr OpenEXR/IlmImfTest/comp_dwaa_v1.exr OpenEXR/IlmImfTest/comp_dwaa_v2.exr OpenEXR/IlmImfTest/comp_dwaa_v1.exr OpenEXR/IlmImfTest/comp_dwaa_v2.exr OpenEXR/IlmImfTest/comp_dwab_v1.exr OpenEXR/IlmImfTest/comp_dwab_v2.exr OpenEXR/IlmImfTest/comp_dwab_v1.exr OpenEXR/IlmImfTest/comp_dwab_v2.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_invalid_unknown.exr OpenEXR/IlmImfTest/comp_none.exr OpenEXR/IlmImfTest/comp_piz.exr OpenEXR/IlmImfTest/comp_rle.exr OpenEXR/IlmImfTest/scanline_with_deepscanline_type.exr OpenEXR/IlmImfTest/scanline_with_deeptiled_type.exr OpenEXR/IlmImfTest/scanline_with_tiledimage_type.exr OpenEXR/IlmImfTest/comp_zip.exr OpenEXR/IlmImfTest/comp_zips.exr OpenEXR/IlmImfTest/compareB44.cpp OpenEXR/IlmImfTest/compareB44.h OpenEXR/IlmImfTest/compareB44.cpp OpenEXR/IlmImfTest/compareB44.h OpenEXR/IlmImfTest/compareFloat.cpp OpenEXR/IlmImfTest/compareFloat.h OpenEXR/IlmImfTest/compareFloat.cpp OpenEXR/IlmImfTest/compareFloat.h OpenEXR/IlmImfTest/lineOrder_decreasing.exr OpenEXR/IlmImfTest/lineOrder_increasing.exr OpenEXR/IlmImfTest/testConversion.cpp OpenEXR/IlmImfTest/testCopyMultiPartFile.cpp OpenEXR/IlmImfTest/testFutureProofing.cpp OpenEXR/IlmImfTest/testInputPart.cpp OpenEXR/IlmImfTest/testMultiPartFileMixingBasic.cpp OpenEXR/IlmImfTest/testCopyMultiPartFile.cpp OpenEXR/IlmImfTest/testFutureProofing.cpp OpenEXR/IlmImfTest/testInputPart.cpp OpenEXR/IlmImfTest/testMultiPartFileMixingBasic.cpp OpenEXR/IlmImfTest/testCopyMultiPartFile.cpp OpenEXR/IlmImfTest/testFutureProofing.cpp OpenEXR/IlmImfTest/testInputPart.cpp OpenEXR/IlmImfTest/testMultiPartFileMixingBasic.cpp OpenEXR/IlmImfTest/testCopyMultiPartFile.cpp OpenEXR/IlmImfTest/testFutureProofing.cpp OpenEXR/IlmImfTest/testInputPart.cpp OpenEXR/IlmImfTest/testMultiPartFileMixingBasic.cpp OpenEXR/IlmImfTest/testTiledYa.cpp OpenEXR/IlmImfTest/testTiledYa.h OpenEXR/IlmImfTest/testYca.cpp OpenEXR/IlmImfTest/testYca.h OpenEXR/IlmImfTest/testTiledYa.cpp OpenEXR/IlmImfTest/testTiledYa.h OpenEXR/IlmImfTest/testYca.cpp OpenEXR/IlmImfTest/testYca.h OpenEXR/IlmImfTest/testTiledYa.cpp OpenEXR/IlmImfTest/testTiledYa.h OpenEXR/IlmImfTest/testYca.cpp OpenEXR/IlmImfTest/testYca.h OpenEXR/IlmImfTest/testTiledYa.cpp OpenEXR/IlmImfTest/testTiledYa.h OpenEXR/IlmImfTest/testYca.cpp OpenEXR/IlmImfTest/testYca.h OpenEXR/IlmImfTest/test_native1.exr OpenEXR/IlmImfTest/test_native2.exr OpenEXR/IlmImfTest/test_native1.exr OpenEXR/IlmImfTest/test_native2.exr OpenEXR/IlmImfTest/tiled_with_deepscanline_type.exr OpenEXR/IlmImfTest/tiled_with_deeptile_type.exr OpenEXR/IlmImfTest/tiled_with_scanlineimage_type.exr OpenEXR/IlmImfTest/tiled_with_deepscanline_type.exr OpenEXR/IlmImfTest/tiled_with_deeptile_type.exr OpenEXR/IlmImfTest/tiled_with_scanlineimage_type.exr OpenEXR/IlmImfTest/tiled_with_deepscanline_type.exr OpenEXR/IlmImfTest/tiled_with_deeptile_type.exr OpenEXR/IlmImfTest/tiled_with_scanlineimage_type.exr OpenEXR/IlmImfTest/v1.7.test.1.exr OpenEXR/IlmImfTest/v1.7.test.planar.exr OpenEXR/IlmImfTest/v1.7.test.1.exr OpenEXR/IlmImfTest/v1.7.test.planar.exr OpenEXR/IlmImfTest/v1.7.test.interleaved.exr OpenEXR/IlmImfTest/v1.7.test.tiled.exr OpenEXR/IlmImfUtil/CMakeLists.txt OpenEXR/IlmImfUtil/Makefile.am OpenEXR/IlmImfUtil/CMakeLists.txt OpenEXR/IlmImfUtil/Makefile.am OpenEXR/IlmImfUtilTest/CMakeLists.txt OpenEXR/IlmImfUtilTest/Makefile.am OpenEXR/IlmImfUtilTest/CMakeLists.txt OpenEXR/IlmImfUtilTest/Makefile.am OpenEXR/doc/InterpretingDeepPixels.docx OpenEXR/doc/MultiViewOpenEXR.odt OpenEXR/doc/OpenEXRFileLayout.odt OpenEXR/doc/ReadingAndWritingImageFiles.odt OpenEXR/doc/TechnicalIntroduction.docx OpenEXR/doc/TechnicalIntroduction.odt OpenEXR/exrbuild/exrbuild.cpp OpenEXR/exrenvmap/CMakeLists.txt OpenEXR/exrenvmap/Makefile.am OpenEXR/exrenvmap/CMakeLists.txt OpenEXR/exrenvmap/Makefile.am OpenEXR/exrmakepreview/CMakeLists.txt OpenEXR/exrmakepreview/Makefile.am OpenEXR/exrmakepreview/CMakeLists.txt OpenEXR/exrmakepreview/Makefile.am OpenEXR/exrmaketiled/CMakeLists.txt OpenEXR/exrmaketiled/Makefile.am OpenEXR/exrmaketiled/CMakeLists.txt OpenEXR/exrmaketiled/Makefile.am OpenEXR/exrmultiview/CMakeLists.txt OpenEXR/exrmultiview/Makefile.am OpenEXR/exrmultiview/CMakeLists.txt OpenEXR/exrmultiview/Makefile.am OpenEXR_Viewers/* OpenEXR_Viewers/doc/OpenEXRViewers.odt OpenEXR_Viewers/exrdisplay/CMakeLists.txt OpenEXR_Viewers/exrdisplay/Makefile.am OpenEXR_Viewers/exrdisplay/CMakeLists.txt OpenEXR_Viewers/exrdisplay/Makefile.am OpenEXR_Viewers/exrdisplay/applyCtl.h OpenEXR_Viewers/exrdisplay/loadImage.cpp OpenEXR_Viewers/exrdisplay/loadImage.h OpenEXR_Viewers/exrdisplay/loadImage.cpp OpenEXR_Viewers/exrdisplay/loadImage.h OpenEXR_Viewers/playexr/CMakeLists.txt OpenEXR_Viewers/playexr/Makefile.am OpenEXR_Viewers/playexr/CMakeLists.txt OpenEXR_Viewers/playexr/Makefile.am PyIlmBase/* PyIlmBase/PyIex/CMakeLists.txt PyIlmBase/PyIex/Makefile.am PyIlmBase/PyIex/CMakeLists.txt PyIlmBase/PyIex/Makefile.am PyIlmBase/PyImath/CMakeLists.txt PyIlmBase/PyImath/Makefile.am PyIlmBase/PyImath/CMakeLists.txt PyIlmBase/PyImath/Makefile.am cmake/* doc/* doc/OpenEXRFileLayout.odt doc/ReadingAndWritingImageFiles.odt doc/TechnicalIntroduction.odt
Copyright: 2005, Industrial Light & Magic, a division of Lucas Digital Ltd. LLC
License: ilmbase
